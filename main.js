
const API_KEY = '761515de2dc6457fa91d113795b271c4';
const ENDPOINT_API = 'https://newsapi.org/v2/top-headlines';

function createElementCard(title, url, source, publishedAt, urlImage, description) {
    let cardContainer = document.createElement('div');
    cardContainer.classList.add('card_container');

    let cardBody = document.createElement('div');
    cardBody.classList.add('card_body');

    cardContainer.appendChild(cardBody);

    let imgArticle = document.createElement('img');
    imgArticle.src = urlImage === null ? 'https://shorturl.at/jmo17' : urlImage
    imgArticle.classList.add('img-fluid');
    cardBody.appendChild(imgArticle);

    let sourceArticle = document.createElement('span');
    sourceArticle.textContent = source;
    sourceArticle.classList.add('badge-source');
    cardBody.appendChild(sourceArticle);

    let titleArticle = document.createElement('h4');
    titleArticle.textContent = title;
    cardBody.appendChild(titleArticle);

    let descriptionArticle = document.createElement('p');
    descriptionArticle.textContent = description;
    cardBody.appendChild(descriptionArticle);

    let urlArticle = document.createElement('a');
    urlArticle.href = url;
    urlArticle.textContent = 'Voir l\'article';
    cardBody.appendChild(urlArticle);

    let publishedAtArticle = document.createElement('span');
    publishedAtArticle.style.cssFloat = 'right'
    publishedAtArticle.textContent = publishedAt.slice(0,10);
    cardBody.appendChild(publishedAtArticle);

    let content = document.getElementById('content');
    content.appendChild(cardContainer);
}

function getDataIntoLocalStorage(itemLocalStorage){
    let storedData = localStorage.getItem(itemLocalStorage);
    let parsedData = JSON.parse(storedData);
    let total = document.getElementById('totalResults');

    total.innerHTML= `Il y a ${parsedData.totalResults} articles`;

    parsedData.articles.forEach(article => {
        createElementCard(
            article.title,
            article.url,
            article.source.name,
            article.publishedAt,
            article.urlToImage,
            article.description,
        );
    })
}

function checkoutDataReplaceContent(button, isLocalStorageItem) {
    // si button click === same class of content ==> ALERT SINON remove old show New
    if (document.getElementById('content').classList.contains(isLocalStorageItem)) {
        console.log(isLocalStorageItem, 'same?')
    } else {
        const actionsBlock = document.getElementById('actions');
        const alert = document.createElement('p');

        alert.textContent = 'Article(s) déjà chargé';
        alert.style.color = 'red';

        actionsBlock.appendChild(alert)

        setTimeout(() => {
            alert.remove();
        }, 5000);
    }
}

function refreshData(selectorBtn, localStorageItem, method) {
    let initButton = document.getElementById(selectorBtn);
    initButton.classList.add(localStorageItem);

    document.getElementById(selectorBtn).addEventListener('click', () => {
        let initContent = document.getElementById('content');
        //TODO
        //  init content with name of localStorage by JUST ONE


        if (!document.querySelector('.card_container')) {
            localStorage.getItem(localStorageItem) ? getDataIntoLocalStorage(localStorageItem) : method();
        } else {
            checkoutDataReplaceContent(selectorBtn, localStorageItem);
        }
    })
}


function getTopHeadlineToCountry(country, pageSize, page) {
    console.log(ENDPOINT_API, API_KEY);
    const url = `${ENDPOINT_API}?country=${country}&apiKey=${API_KEY}`;
    const req = new Request(url);

    fetch(req)
        .then((response) => response.json())
        .then((data) => {
            let dataJsonString = JSON.stringify(data);
            localStorage.setItem(`top-headline-country-${country}`, dataJsonString);

            getDataIntoLocalStorage(`top-headline-country-${country}`);
        })
        .catch(console.error)
}

function getTopHeadlineToCountryAndCategory(country, category, pageSize, page) {
    const url = `${ENDPOINT_API}?country=${country}&category=${category}&apiKey=${API_KEY}`;
    const req = new Request(url);

    fetch(req)
        .then((response) => response.json())
        .then((data) => {
            let dataJsonString = JSON.stringify(data);
            localStorage.setItem(`top-headline-${country}-${category}`, dataJsonString);

            getDataIntoLocalStorage(`top-headline-${country}-${category}`);
        })
        .catch(console.error)
}

refreshData('btn-data', `top-headline-country-fr`, function () {
    getTopHeadlineToCountry('fr');
});

refreshData('btn-get-tech-new', `top-headline-fr-technology`, function () {
    getTopHeadlineToCountryAndCategory('fr','technology');
});

refreshData('btn-get-business', `top-headline-fr-business`, function () {
    getTopHeadlineToCountryAndCategory('fr','business');
});

refreshData('btn-get-entertainment', `top-headline-fr-entertainment`, function () {
    getTopHeadlineToCountryAndCategory('fr','entertainment');
});

refreshData('btn-get-general', `top-headline-fr-general`, function () {
    getTopHeadlineToCountryAndCategory('fr','general');
});

refreshData('btn-get-health', `top-headline-fr-health`, function () {
    getTopHeadlineToCountryAndCategory('fr','health');
});

refreshData('btn-get-sciences', `top-headline-fr-sciences`, function () {
    getTopHeadlineToCountryAndCategory('fr','sciences');
});

refreshData('btn-get-sport', `top-headline-fr-sports`, function () {
    getTopHeadlineToCountryAndCategory('fr','sports');
});